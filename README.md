<img src="preview.png" alt="OrangeFox 3D" width="300"/>

# OrangeFox App
App for <a href="https://wiki.orangefox.tech">OrangeFox Recovery</a> written on Java

## Features

* Downloading releases
* Get info about releases
* Update checking in background
* Backups/logs management
* OpenRecoveryScript creation
* Using API v3

## Used libraries

* [AndroidX](https://developer.android.com/jetpack/androidx)
* [Material Components](https://material.io)
* [AHBottomNavigation](https://github.com/aurelhubert/ahbottomnavigation)
* [SmartTabLayout](https://github.com/ogaclejapan/SmartTabLayout)
* [Facebook Shimmer](https://github.com/facebook/shimmer-android)
* [OkHttp](https://square.github.io/okhttp/)
* [HaulerView](https://github.com/futuredapp/hauler)
* [Overscroll Decor](https://github.com/EverythingMe/overscroll-decor)
* [libsu](https://github.com/topjohnwu/libsu)
* [LeakCanary](https://github.com/square/leakcanary)

## Build

You need installed Android Studio v4.1.1 and SDK 30

## License

```
Copyright 2020 fordownloads

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
